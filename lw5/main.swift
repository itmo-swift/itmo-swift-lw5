//
//  main.swift
//  lw5
//
//  Created by Daniil Lushnikov on 27.09.2021.
//  Switch case of entered line

// f - marker of case (lowercase - l, uppercase - u)
let f = readLine() ?? ""
// line for changing case
let l = readLine() ?? ""
changeInputLine(action: f, line:l)

func changeInputLine(action: String, line: String) -> Void {
    switch action {
        case "l":
            print(line.lowercased())
        case "u":
            print(line.uppercased())
        default:
            print("Wrong marker. Should be l or u (lowercase or uppercase)")
    }
}
